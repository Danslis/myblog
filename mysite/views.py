

from django.shortcuts import get_object_or_404, render
from django.template import loader, Context
from django.http import HttpResponse
from blog.models import BlogPost


def index(request):
    posts = BlogPost.objects.all()
    #t = loader.get_template('archive.html')
    context={'posts':posts}
    return render(request, 'archive.html', context)
    #return HttpResponse("Hello, world. You're at the polls index.")
    #return HttpResponse(t.render(c))
    #return render(request, 'archive.html')

