from django.db import models
from django.contrib import admin
#from blog.models import BlogPost


#from django.contrib import admin

class BlogPost(models.Model):
    title = models.CharField(max_length= 150)
    body = models.TextField()
    timestamp = models.DateTimeField()

admin.site.register(BlogPost)

#class BlogPostAdmin(admin.Model):
    #list_display = ('title','timestamp')


#admin.site.register(BlogPost)
